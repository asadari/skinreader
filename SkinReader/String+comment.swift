//
//  String+comment.swift
//  SkinReader
//
//  Created by Gwanho on 2017. 1. 12..
//  Copyright © 2017년 skinReader. All rights reserved.
//

import UIKit

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func localizedWithComment(comment: String) -> String {
        return NSLocalizedString(self, comment:comment)
    }
}
