import Foundation

/**
 Returns a `Dictionary` of `String` keys and `String` values for each key/value pair in an `NSURL` query string.
 - author: Scott Gardner
 - seealso:
 * [Source on GitHub](http://bit.ly/SwiftQueryItemsDictionaryNSURLExtension)
 */
extension NSURL {
  
  var queryItemsDictionary: [String: String] {
    var queryItemsDictionary = [String: String]()
    guard let components = NSURLComponents(url: self as URL, resolvingAgainstBaseURL: false), let queryItems = components.queryItems else { return queryItemsDictionary }
    queryItems.forEach { queryItemsDictionary[$0.name] = $0.value }
    return queryItemsDictionary
  }
  
}

extension URL {
    var queryItemsDictionary: [String: String] {
        var queryItemsDictionary = [String: String]()
        guard let components = URLComponents(url: self, resolvingAgainstBaseURL: false), let queryItems = components.queryItems else {
            return queryItemsDictionary
        }
        
        queryItems.forEach{queryItemsDictionary[$0.name] = $0.value }
        return queryItemsDictionary
    }
}
