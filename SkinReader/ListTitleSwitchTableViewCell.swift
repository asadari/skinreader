//
//  ListTitleSwitchTableViewCell.swift
//  SkinReader
//
//  Created by Gwanho on 2017. 2. 1..
//  Copyright © 2017년 skinReader. All rights reserved.
//

import UIKit

protocol ListTitleSwitchCellDelegate {
    func listSwichCell(view : ListTitleSwitchTableViewCell, SwitchValueChanged isOn : Bool, IndexPath indexPath : IndexPath)
    
}

class ListTitleSwitchTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var onOffSwitch : UISwitch!
    var indexPath : IndexPath! = IndexPath()
    
    var delegate : ListTitleSwitchCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func swiftOnOffChangedValue(){
    
        if let delegate = self.delegate {
            delegate.listSwichCell(view: self, SwitchValueChanged: self.onOffSwitch.isOn, IndexPath: self.indexPath)
        }
    }
    
}
