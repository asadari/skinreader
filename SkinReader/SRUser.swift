//
//  SRUser.swift
//  SkinReader
//
//  Created by Gwanho on 2017. 1. 18..
//  Copyright © 2017년 skinReader. All rights reserved.
//


import UIKit

class SRUser {
    static let instance = SRUser()
    
    var initUrlString : String = ServerManager.getPageUrl(type: .home)
    
    
    var isLogin : Bool {
        get {
            guard let tmpEmail = SRUtils.getObjectBool(key: StringValue.isLogin.rawValue) as Bool? else {
                return false
            }
            return tmpEmail
        }
        
        set (newVale) {
            SRUtils.setObjectBool(object: newVale, Key: StringValue.isLogin.rawValue)
        }
    }
    
    var email : String {
        get {
            guard let tmpEmail = SRUtils.getObjectString(key: StringValue.email.rawValue) as String? else {
                return ""
            }
            return tmpEmail
        }
        
        set (newVale) {
            SRUtils.setObjectString(object: newVale, Key: StringValue.email.rawValue)
        }
    }

    var idno : String {
        get {
            guard let tmpIdno = SRUtils.getObjectString(key: StringValue.idno.rawValue) as String? else {
                return ""
            }
            return tmpIdno
        }
        
        set (newVale) {
            SRUtils.setObjectString(object: newVale, Key: StringValue.idno.rawValue)
        }
    }
    
    var idtype : String {
        get {
            guard let tmpValue = SRUtils.getObjectString(key: StringValue.idtype.rawValue) as String? else {
                return ""
            }
            return tmpValue
        }
        
        set (newVale) {
            SRUtils.setObjectString(object: newVale, Key: StringValue.idtype.rawValue)
        }
    }
    
    var nickname : String {
        get {
            guard let tmpValue = SRUtils.getObjectString(key: StringValue.nickname.rawValue) as String? else {
                return "Guest"
            }
            
            if tmpValue == "" {
                self.nickname = "Guest"
                return "Guest"
            }
            
            return tmpValue
        }
        
        set (newVale) {
            SRUtils.setObjectString(object: newVale, Key: StringValue.nickname.rawValue)
        }
    }
    
    var skintype_text : String {
        get {
            guard let tmpValue = SRUtils.getObjectString(key: StringValue.skintype_text.rawValue) as String? else {
                return ""
            }
            return tmpValue
        }
        
        set (newVale) {
            SRUtils.setObjectString(object: newVale, Key: StringValue.skintype_text.rawValue)
        }
    }
    
    var image : String {
        get {
            guard let tmpValue = SRUtils.getObjectString(key: StringValue.image.rawValue) as String? else {
                return ""
            }
            return tmpValue
        }
        
        set (newVale) {
            SRUtils.setObjectString(object: newVale, Key: StringValue.image.rawValue)
        }
    }
    
    var push : String {
        get {
            guard let tmpValue = SRUtils.getObjectString(key: StringValue.push.rawValue) as String? else {
                return "on"
            }
            
            if tmpValue == "" {
                self.push = "on"
            }
            
            return tmpValue
        }
        
        set (newVale) {
            SRUtils.setObjectString(object: newVale, Key: StringValue.push.rawValue)
        }
    }
    

    
    var deviceUUID : String  {
        get {
            guard let tmpValue = SRUtils.getObjectString(key: StringValue.uuid.rawValue) as String? else {
                let uuid = UIDevice.current.identifierForVendor?.uuidString
                SRUtils.setObjectString(object: uuid!, Key: StringValue.uuid.rawValue)
                return uuid!
            }
            
            if tmpValue == "" {
                let uuid = UIDevice.current.identifierForVendor?.uuidString
                SRUtils.setObjectString(object: uuid!, Key: StringValue.uuid.rawValue)
                return uuid!
            }
            
            return tmpValue
        }

    }
    
    var userPushKey : String  {
        get {
            guard let tmpValue = SRUtils.getObjectString(key: StringValue.pushKey.rawValue) as String? else {
                return ""
            }
            
            return tmpValue
        }

        set (newVale) {
            SRUtils.setObjectString(object: newVale, Key: StringValue.pushKey.rawValue)
        }
    }
    
    var user_idx : String {
        get {
            guard let tmpValue = SRUtils.getObjectString(key: StringValue.user_idx.rawValue) as String? else {
                return ""
            }
            return tmpValue
        }
        
        set (newVale) {
            SRUtils.setObjectString(object: newVale, Key: StringValue.user_idx.rawValue)
        }
    }
    
    var idTypeValue : String {
        get{
            if self.idtype == "" {
                return "Guest"
            } else if self.idtype == "1" {
                return "네이버로 연결됨"
            } else if self.idtype == "2" {
                return "카카오톡으로 연결됨"
            } else if self.idtype == "3" {
                return "페이스북으로 연결됨"
            } else  {
                return "Guest"
            }
        }
    }
    
    init() {
        
    }
    
    func setUserDataFromJson(info : Dictionary<String,AnyObject>) {
        
    }
    
}
