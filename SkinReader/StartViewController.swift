//
//  StartViewController.swift
//  SkinReader
//
//  Created by Gwanho on 2017. 1. 19..
//  Copyright © 2017년 skinReader. All rights reserved.
//

import UIKit
import  Alamofire
import SwiftyJSON

protocol StartViewDelegate {
    func StartView(_ view : StartViewController, IsSuccess isSuccsess : Bool)
}

class StartViewController: UIViewController {
    var webView : UIWebView = UIWebView()
    var delegate : StartViewDelegate? = nil
    
    static var isAutoLogin = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.delegate = self;
        StartViewController.httpReqeustAutoLogin { (isSuccess) in
            self.dismiss(animated: true, completion: {
                if let delegate = self.delegate {
                    delegate.StartView(self, IsSuccess: isSuccess)
                    StartViewController.isAutoLogin = true
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func gotoMain()  {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
//        //        let url = URL(string: STR_baseUrl)
//        //        mainViewController.url = url!
//        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
//        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
//        UINavigationBar.appearance().tintColor = UIColor.red
//        
//        leftViewController.mainViewController = nvc
//        
//        let slideMenuController = ContainerViewController(mainViewController:nvc, leftMenuViewController: leftViewController)
//        slideMenuController.automaticallyAdjustsScrollViewInsets = true
//        slideMenuController.delegate = leftViewController
//        
//        AppDelegate.application
//    }
    
    class func httpReqeustAutoLogin(Complete callback:@escaping (Bool) -> Void) {
        let url = URL(string: ServerManager.getPageUrl(type: .autoLogin))
        var param = Dictionary<String,String>()
        param[StringValue.idno.rawValue] = SRUser.instance.idno
        param[StringValue.idtype.rawValue] = SRUser.instance.idtype
        Alamofire.request(url!, method: .post, parameters: param, encoding: URLEncoding.default, headers:ServerManager.getHttpHeader()).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                if let user = json[StringValue.user.rawValue].dictionary {
                    if let email = user[StringValue.email.rawValue]{
                        SRUser.instance.email = email.stringValue;
                    }
                    if let idno = user[StringValue.idno.rawValue]{
                        SRUser.instance.idno = idno.stringValue;
                    }
                    
                    if let idtype = user[StringValue.idtype.rawValue]{
                        SRUser.instance.idtype = idtype.stringValue;
                    }
                    
                    if let image = user[StringValue.image.rawValue]{
                        SRUser.instance.image = image.stringValue;
                    }
                    
                    if let nickname = user[StringValue.nickname.rawValue]{
                        SRUser.instance.nickname = nickname.stringValue;
                    }
                    
                    if let skintype_text = user[StringValue.skintype_text.rawValue]{
                        SRUser.instance.skintype_text = skintype_text.stringValue;
                    }
                    
                    if let user_idx = user[StringValue.user_idx.rawValue]{
                        SRUser.instance.user_idx = user_idx.stringValue;
                    }
                    
                    if let isLogin = user[StringValue.isLogin.rawValue]?.bool {
                        SRUser.instance.isLogin = isLogin
                    }
                }
                callback(true )

            case .failure(let error):
                print(error)
                callback(false)
            }
        }
    }

}

extension StartViewController : UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let url = request.mainDocumentURL, let scheme = url.scheme  {
            
            return false
        }
        return true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
    }
}
