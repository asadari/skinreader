//
//  LeftTableviewHeaderView.swift
//  SkinReader
//
//  Created by Gwanho on 2017. 1. 11..
//  Copyright © 2017년 skinReader. All rights reserved.
//

import UIKit

@objc protocol LeftTableViewHeaderDelegate {
    func leftTableViewHeaderViewDidPressProfile(view : LeftTableviewHeaderView)
    func leftTableViewHeaderViewDidPressStartTest(view : LeftTableviewHeaderView)
}

class LeftTableviewHeaderView: UIView {
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var profileButton : UIButton!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var conditionLabel : UILabel!
    var delegate : LeftTableViewHeaderDelegate? = nil

    
    @IBAction func profileButtonPressed(object : AnyObject) {
        if let delegate = self.delegate {
            delegate.leftTableViewHeaderViewDidPressProfile(view: self)
        }
    }
    
    @IBAction func startTestAction(object : AnyObject) {
        if let delegate = self.delegate {
            delegate.leftTableViewHeaderViewDidPressStartTest(view: self)
        }
    }
}
