//
//  ServerManager.swift
//  SkinReader
//
//  Created by Gwanho on 2017. 1. 17..
//  Copyright © 2017년 skinReader. All rights reserved.
//

import UIKit



let STR_baseUrl:String = "http://\(STR_host)"
let STR_host : String = "app.skin16.com"

class ServerManager: NSObject {
    class func getServerBaseUrl() -> String{
        return   STR_baseUrl
    }
    
    class func getPageUrl(type : PageUrl) -> String  {
        return "\(self.getServerBaseUrl())\(type.rawValue)"
    }
    
    class func getRequestWithWebViewHeader(_ url : URL!, Body body :String ) -> NSMutableURLRequest {
        
        let headerInfo = self.getHttpHeader()
        let keyVersion = HeaderKey.f_version.rawValue
        let keyOs = HeaderKey.f_os.rawValue
        let keyDeviceId = HeaderKey.f_device_id.rawValue
        let keyMode = HeaderKey.f_model.rawValue
        let keyLanguage = HeaderKey.f_language.rawValue
        let keyPush = HeaderKey.f_push.rawValue
        let keyIdType = HeaderKey.idtype.rawValue
        let keyPushKey = HeaderKey.f_push_key.rawValue
        let keyIdno = HeaderKey.idno.rawValue
        let keyUserIdx = HeaderKey.f_user_idx.rawValue

        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = body.data(using: .utf8)
        request.addValue(headerInfo[keyVersion]!, forHTTPHeaderField: keyVersion)
        request.addValue(headerInfo[keyOs]!, forHTTPHeaderField: keyOs)
        request.addValue(headerInfo[keyDeviceId]!, forHTTPHeaderField: keyDeviceId)
        request.addValue(headerInfo[keyMode]!, forHTTPHeaderField: keyMode)
        request.addValue(headerInfo[keyLanguage]!, forHTTPHeaderField: keyLanguage)
        request.addValue(headerInfo[keyPush]!, forHTTPHeaderField: keyPush)
        request.addValue(headerInfo[keyIdType]!, forHTTPHeaderField: keyIdType)
        request.addValue(headerInfo[keyPushKey]!, forHTTPHeaderField: keyPushKey)
        request.addValue(headerInfo[keyIdno]!, forHTTPHeaderField: keyIdno)
        request.addValue(headerInfo[keyUserIdx]!, forHTTPHeaderField: keyUserIdx)
        return request
    }
    
    class func getHttpHeader() -> Dictionary<String,String> {
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"]
        let currentDevice = UIDevice.current
        
        var info = Dictionary<String,String>()
        info[HeaderKey.f_version.rawValue] = version as? String
        info[HeaderKey.f_os.rawValue] = "ios"
        info[HeaderKey.f_device_id.rawValue] = SRUser.instance.deviceUUID
        info[HeaderKey.f_model.rawValue] = currentDevice.model
        info[HeaderKey.f_language.rawValue] = Locale.current.languageCode!
        info[HeaderKey.f_push.rawValue] = SRUser.instance.push
        info[HeaderKey.f_push_key.rawValue] = SRUser.instance.userPushKey
        info[HeaderKey.idtype.rawValue] = SRUser.instance.idtype
        info[HeaderKey.idno.rawValue] = SRUser.instance.idno
        info[HeaderKey.f_user_idx.rawValue] = SRUser.instance.user_idx
        return info
    }

}
