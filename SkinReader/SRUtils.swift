//
//  SRUtils.swift
//  SkinReader
//
//  Created by Gwanho on 2017. 1. 17..
//  Copyright © 2017년 skinReader. All rights reserved.
//

import UIKit
//import KeychainAccess
import KeychainSwift



enum StringValue : String {
    case login
    case cosmeticreader
    case jsonData
    case image
    case email
    case idno
    case idtype
    case nickname
    case skintype_text
    case file
    case upload
    case user
    case push
    case uuid
    case pushKey
    case user_idx
    case result
    case isLogin
}

enum HeaderKey : String {
    case f_os
    case f_device_id
    case f_language
    case f_model
    case f_version
    case f_push_key
    case f_user_idx
    case f_push
    case idtype
    case idno
}
enum PageUrl:String{
    case home = "/"
    case movie = "/product/product_list/movie/"
    case magazine = "/magazine/magazine/"
    case diary = "/skin_diary/skin_diary/"
    case myList = "/member/favorite/"
    case notice = "/notice/notice/"
    case faq = "/notice/notice/faq/"
    case oneonene = "/notice/oneonone/"
    case autoLogin = "/app_api/login/"
    case login = "/member/login/"
    case pushSet = "/app_api/push_set/"
    case pimples = "/processing/pimples_regi/"
    case pores = "/processing/pores_regi/"
    case skinReader = "/skin_reader/skin_reader/"
    case myhome = "/member/myhome/"
    case test
    case setting = "setting"

}

enum javaScriptUrl:String {
    case logOut = "javascript:app_sns_logout()"
    case loginScheme = "javascript:app_sns_login"
}

let keyChainId : String = "cosmeticreader"

class SRUtils: NSObject {
        
    class func setObjectData(object : Data, Key key:String) {
        let keychain = KeychainSwift()
        keychain.set(object, forKey: key)
    }
    
    class func setObjectBool(object : Bool, Key key:String) {
        let keychain = KeychainSwift()
        keychain.set(object, forKey: key)
    }
    
    class func setObjectString(object : String, Key key:String) {
        let keychain = KeychainSwift()
        keychain.set(object, forKey: key)
    }
    
    class func getObjectString(key:String)->String {
        let keychain = KeychainSwift()
        guard  let object = keychain.get(key) else {
            return ""
        }
        return object
    }
    
    class func getObjectBool(key:String)->Bool {
        let keychain = KeychainSwift()
        guard  let object = keychain.getBool(key) else {
            return false
        }
        return object
    }
    
    class func resetKeyChainData() {
        let keychain = KeychainSwift()
        keychain.clear()
    }
    
    class func resetUserData() {
        let keyChain = KeychainSwift()
        keyChain.set("", forKey: StringValue.email.rawValue)
        keyChain.set("", forKey: StringValue.idno.rawValue)
        keyChain.set("", forKey: StringValue.nickname.rawValue)
        keyChain.set("", forKey: StringValue.image.rawValue)
        keyChain.set("", forKey: StringValue.user_idx.rawValue)
        keyChain.set("", forKey: StringValue.idtype.rawValue)
    }
}

extension Notification.Name {
    static let remotePushNotiName = Notification.Name("remotePushNotiName")
    static let autoLoginComplete = Notification.Name("autoLoginComplete")
}
