//
//  ContainerViewController.swift
//  MetaRule
//
//  Created by Gwanho on 2017. 1. 9..
//  Copyright © 2017년 lovenfree. All rights reserved.
//

import UIKit
//import SlideMenuControllerSwift
class ContainerViewController: SlideMenuController {

//    override func awakeFromNib() {
//        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") {
//            self.mainViewController = controller
//        }
//        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") {
//            self.leftViewController = controller
//        }
//        super.awakeFromNib()
//    }
    
    
    override func isTagetViewController() -> Bool {
        if let vc = UIApplication.topViewController() {
            if vc is MainViewController {
                return true
            }
        }
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func track(_ trackAction: TrackAction) {
        switch trackAction {
        case .leftTapOpen:
            print("TrackAction: left tap open.")
        case .leftTapClose:
            print("TrackAction: left tap close.")
        case .leftFlickOpen:
            print("TrackAction: left flick open.")
        case .leftFlickClose:
            print("TrackAction: left flick close.")
        case .rightTapOpen:
            print("TrackAction: right tap open.")
        case .rightTapClose:
            print("TrackAction: right tap close.")
        case .rightFlickOpen:
            print("TrackAction: right flick open.")
        case .rightFlickClose:
            print("TrackAction: right flick close.")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
