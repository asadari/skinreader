//
//  SettingViewController.swift
//  SkinReader
//
//  Created by Gwanho on 2017. 1. 12..
//  Copyright © 2017년 skinReader. All rights reserved.
//

import UIKit
import  Alamofire
import SwiftyJSON
enum SettingCellIdentifier : String {
    case subTitleRightCell
    case detailButtonCell
    case onOffCell
    case listCell
}

class SettingCellInfo : NSObject {
    var title : String = ""
    var subTitle : String = ""
    var cellType : SettingCellIdentifier = .subTitleRightCell
    var onOff : Bool = false
}

@objc protocol SettingViewDelegate {
    func SettingView(view : SettingViewController, IndexPath indexPath : IndexPath)
}

class SettingViewController: UIViewController {
    
    enum Section : Int{
        case defaultSetion = 0
        static let count = 1
    }
    
    enum CellIdentifier : String {
        case subTitleRightCell
        case detailButtonCell
        case onOffCell
    }
    
    @IBOutlet weak var tableView : UITableView!
    var listArray : Array<SettingCellInfo> = Array()
    var delegate : SettingViewDelegate?
    @IBOutlet weak var titleLabel : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "설정"
        self.setdefaultData()
        self.tableView.register(UINib(nibName: "ListTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "listCell")
        self.tableView.register(UINib(nibName: "ListTileSubTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "subTitleRightCell")
        self.tableView.register(UINib(nibName: "ListTitleSwitchTableViewCell", bundle: nil), forCellReuseIdentifier: "onOffCell")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK : DataSetting
extension SettingViewController {
    class func baseSettingViewControllerFromStroyBoard() -> SettingViewController {
        let stroyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = stroyboard.instantiateViewController(withIdentifier: "SettingViewController")
        return viewController as! SettingViewController
    }
    
    func setdefaultData() {
        var cellInfo = SettingCellInfo()
        cellInfo.title = "계정정보"
        cellInfo.subTitle = SRUser.instance.idTypeValue
        cellInfo.cellType = .subTitleRightCell
        listArray.append(cellInfo)
        
        cellInfo = SettingCellInfo()
        cellInfo.title = "공지사항"
        cellInfo.subTitle = ""
        cellInfo.cellType = .detailButtonCell
        listArray.append(cellInfo)
        
        cellInfo = SettingCellInfo()
        cellInfo.title = "FAQ"
        cellInfo.subTitle = ""
        cellInfo.cellType = .detailButtonCell
        listArray.append(cellInfo)
        
        cellInfo = SettingCellInfo()
        cellInfo.title = "1:1문의"
        cellInfo.subTitle = ""
        cellInfo.cellType = .detailButtonCell
        listArray.append(cellInfo)
        
        cellInfo = SettingCellInfo()
        cellInfo.title = "푸쉬설정"
        cellInfo.subTitle = ""
        cellInfo.cellType = .onOffCell
        cellInfo.onOff = SRUser.instance.push == "on" ? true : false
        listArray.append(cellInfo)
    }
    
    @IBAction func closeButtonPressed(object : AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SettingViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellInfo = listArray[indexPath.row]
        
        if cellInfo.cellType == .subTitleRightCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingCellIdentifier.subTitleRightCell.rawValue, for: indexPath) as! ListTileSubTitleTableViewCell
            cell.titleLabel.text = cellInfo.title
            cell.subtitleLabel.text = cellInfo.subTitle
            cell.selectionStyle = .none
            return cell
        } else if cellInfo.cellType == .detailButtonCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingCellIdentifier.listCell.rawValue, for: indexPath) as! ListTitleTableViewCell
            //        let title = myPageArray[indexPath.row]
            cell.titleLabel.text = cellInfo.title
            cell.selectionStyle = .none
            return cell
        } else if cellInfo.cellType == .onOffCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingCellIdentifier.onOffCell.rawValue, for: indexPath) as! ListTitleSwitchTableViewCell
            cell.titleLabel.text = cellInfo.title
            cell.onOffSwitch.isOn = cellInfo.onOff
            cell.indexPath = indexPath
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingCellIdentifier.listCell.rawValue, for: indexPath) as! ListTitleTableViewCell
            //        let title = myPageArray[indexPath.row]
            cell.titleLabel.text = ""
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cellInfo = listArray[indexPath.row]
        if cellInfo.cellType == .onOffCell {
            return
        }
        
        if let delegate = self.delegate {
            delegate.SettingView(view: self, IndexPath: indexPath)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension SettingViewController : ListTitleSwitchCellDelegate{
    func listSwichCell(view: ListTitleSwitchTableViewCell, SwitchValueChanged isOn: Bool, IndexPath indexPath : IndexPath) {
        
        guard let cellInfo = listArray[indexPath.row] as? SettingCellInfo else {
            return
        }

        let url = URL(string: ServerManager.getPageUrl(type: .pushSet))
        var param = Dictionary<String,String>()
        param[StringValue.idno.rawValue] = SRUser.instance.idno
        param[StringValue.idtype.rawValue] = SRUser.instance.idtype
        var headerInfo = ServerManager.getHttpHeader()
        headerInfo[HeaderKey.f_push.rawValue] = isOn == true ? "on" : "off"
        Alamofire.request(url!, method: .get, parameters: nil, encoding: URLEncoding.default, headers:headerInfo).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                SRUser.instance.push =  isOn == true ? "on" : "off"
                cellInfo.onOff = isOn
                
            case .failure(let error):
                print(error)
                SRUser.instance.push =  isOn == true ? "off" : "on"
                cellInfo.onOff = !isOn
            }
            self.tableView.reloadData()
        }
 
    }
}
