//
//  ViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit

class MainViewController: UIViewController {
    @IBOutlet weak var webView : UIWebView?
    @IBOutlet weak var menuButton : UIButton!
    @IBOutlet weak var backButton : UIButton!
    @IBOutlet weak var titleLabel : UILabel!
    var url : URL = URL(string: SRUser.instance.initUrlString)! {
        didSet {
            let request = ServerManager.getRequestWithWebViewHeader(url,Body:"")
            self.webView?.loadRequest(request as URLRequest)
            print("url = \(url)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notiCenter = NotificationCenter.default
        notiCenter.addObserver(self, selector: #selector(MainViewController.handleRmoetePushNotification(notification:)), name: .remotePushNotiName, object: nil)
        
        notiCenter.addObserver(self, selector: #selector(MainViewController.handleautoLoginCompleteNotification(notification:)), name: .autoLoginComplete, object: nil)
        
        self.url = URL(string: SRUser.instance.initUrlString)!
        let request = ServerManager.getRequestWithWebViewHeader(self.url, Body:"")
        self.webView?.loadRequest(request as URLRequest)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
        viewController.delegate = self
        self.present(viewController, animated: false, completion: nil)
    

    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        //        self.setNavigationBarItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK : FUNC
extension MainViewController {
    @IBAction func menuButtonPressed(object : AnyObject) {
        self.toggleLeft()
    }
    
    @IBAction func backButtonPressed(object : AnyObject) {
        if self.webView?.canGoBack == true {
            self.webView?.goBack()
        }
    }
    
    @IBAction func forwardButtonPressed(object : AnyObject) {
        if self.webView?.canGoForward == true {
            self.webView?.goForward()
        }
    }
    
    @IBAction func reloadButtonPressed(object : AnyObject) {
//        if self.webView?.canGoBack == true {
            self.webView?.reload()
//        }
    }
    
    @IBAction func homeButtonPressed(object : AnyObject) {
        self.url = URL(string: ServerManager.getPageUrl(type: .home))!
    }
    
    func logout() {
        //       let _ = self.webView?.stringByEvaluatingJavaScript(from:"localStorage.clear();")
        
        //        [self.webView stringByEvaluatingJavaScriptFromString:@"javascript:app_sns_logout()”];
        
        
        let _ = self.webView?.stringByEvaluatingJavaScript(from:javaScriptUrl.logOut.rawValue)
        
        
        let storage = HTTPCookieStorage.shared
        
        if let cookies = storage.cookies {
            for cookie in cookies {
                storage.deleteCookie(cookie)
            }
        }
        UserDefaults.standard.synchronize()
        SRUtils.resetUserData()
        
    }
    
    func userLoginDataSeting(From url:URL) {
        if let queryItemDic = url.queryItemsDictionary as? [String:String]{
            if let jsonData = queryItemDic[StringValue.jsonData.rawValue] {
                if let jsonDic = self.convertJsonToDictionary(jsonData: jsonData) {
                    
                    if let email = jsonDic[StringValue.email.rawValue] as? String {
                        SRUser.instance.email = email;
                    }
                    if let idno = jsonDic[StringValue.idno.rawValue] as? String {
                        SRUser.instance.idno = idno;
                    }
                    if let idtype = jsonDic[StringValue.idtype.rawValue] as? String {
                        SRUser.instance.idtype = idtype;
                    }
                    if let user_idx = jsonDic[StringValue.user_idx.rawValue] as? String {
                        SRUser.instance.user_idx = user_idx
                    }
                    if let image = jsonDic[StringValue.image.rawValue] as? String {
                        SRUser.instance.image = image;
                    }
                    if let nickname = jsonDic[StringValue.nickname.rawValue] as? String {
                        SRUser.instance.nickname = nickname;
                    }
                    if let skintype_text = jsonDic[StringValue.skintype_text.rawValue] as? String {
                        SRUser.instance.skintype_text = skintype_text;
                    }
                }
            }
        }
    }
}

//MARK : NoticationCenter Function
extension MainViewController {
    func handleRmoetePushNotification(notification : Notification) {
        guard let userInfo = notification.userInfo  else {
            return
        }
        
        if let urlString = userInfo["webUrl"] as? String  {
            self.url = URL(string: urlString)!
        }
    }
    
    func handleautoLoginCompleteNotification(notification : Notification) {
        let urlString = "\(javaScriptUrl.loginScheme.rawValue)(\(SRUser.instance.idtype),\(SRUser.instance.idno))"
        let _ = self.webView?.stringByEvaluatingJavaScript(from:urlString)
    }
}

// MARK : UIWebViewDelegate

extension MainViewController : UIWebViewDelegate {
    
    func convertJsonToDictionary(jsonData:String) -> Dictionary<String,AnyObject>?  {
        var dictonary:Dictionary<String,AnyObject>?
        if let data = jsonData.data(using: String.Encoding.utf8) {
            
            do {
                dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]

            } catch let error as NSError {
                print(error)
            }
        }
        
        return dictonary
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if let url = request.mainDocumentURL, let scheme = url.scheme  {
            if scheme == StringValue.cosmeticreader.rawValue {
                if url.host == StringValue.login.rawValue {
                    self.userLoginDataSeting(From: url)
                }
                
                if url.host == StringValue.upload.rawValue {
                    self.showCameraAction()
                }
                
                if url.host == StringValue.file.rawValue {
                    self.userLoginDataSeting(From: url)
                }
                return false
            }
        }
        
        if let url = request.mainDocumentURL {
            if request.allHTTPHeaderFields?[HeaderKey.f_os.rawValue] == nil && request.url?.host == "STR_host"{
                DispatchQueue.global(qos: .default).async {
                    DispatchQueue.main.async {
                        self.webView?.loadRequest(ServerManager.getRequestWithWebViewHeader(url, Body: "") as URLRequest)
                    }
                }
                return false
            }
        }
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        titleLabel?.text = webView.stringByEvaluatingJavaScript(from: "document.title")
        
        if StartViewController.isAutoLogin == true {
            let urlString = "\(javaScriptUrl.loginScheme.rawValue)(\(SRUser.instance.idtype),\(SRUser.instance.idno))"
            let _ = self.webView?.stringByEvaluatingJavaScript(from:urlString)
            StartViewController.isAutoLogin = false
        }
        if self.webView?.canGoBack == true{
        } else {
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print(error)
    }
    

}

//MARK : Camera

extension MainViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func showCamera(With type : UIImagePickerControllerSourceType) {
        let imagePicker = UIImagePickerController.init()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
        } else if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
        }
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func showCameraAction(){        
        let alertController = UIAlertController(title: "사진", message: "", preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "camera", style: .default) { (action) in
            self.showCamera(With: .camera)
        }
        
        let photoLibrary = UIAlertAction(title: "photoLibrary", style: .default) { (action) in
            self.showCamera(With: .photoLibrary)
        }
        
        let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        alertController.addAction(camera)
        alertController.addAction(photoLibrary)
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage]
        let data = UIImageJPEGRepresentation(image as! UIImage, 0.8)
        
        dismiss(animated: true, completion: nil)
        
    }
}


// MARK : StartViewDelegate

extension MainViewController : StartViewDelegate {
    func StartView(_ view: StartViewController, IsSuccess isSuccsess: Bool) {
//        self.webView?.reload()
//        switch isSuccsess {
//        case true:
//            self.url = URL(string: ServerManager.getPageUrl(type: .home))!
//        default:
//            break
//        }
    }
}


