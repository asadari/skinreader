//
//  ViewController.swift
//  MetaRule
//
//  Created by Gwanho on 2017. 1. 4..
//  Copyright © 2017년 lovenfree. All rights reserved.
//

import UIKit
import Photos
import Alamofire
import AlamofireImage
//import SlideMenuControllerSwift
enum LeftMenu: Int {
    case collection = 0
    case main
    case java
    case go
    case nonMenu
}


class LeftViewController: UIViewController {

    enum Section : Int {
        case menu = 0
        case myPage
        case setting
        static let count = 3
    }
    
    enum CellIdentifier: String {
        case listCell
    }
    
    enum SegueIdentifier : String {
        case showAllPhotos
        case showCollection
    }
    
    var menuArray = [NSLocalizedString("홈", comment: ""), NSLocalizedString("영상 ∙ 기초화장품", comment: ""), NSLocalizedString("매거진", comment: ""), NSLocalizedString("스킨다이어리", comment: ""), NSLocalizedString("여드름 피부 테스트", comment: ""), NSLocalizedString("모공 피부 테스트", comment: "")]
    var myPageArray = [NSLocalizedString("마이 리스트", comment: ""), NSLocalizedString("설정", comment: "")]
    
    var settingArray = [NSLocalizedString("로그아웃", comment: "")]
    let sectionLocalizedTitles = ["", NSLocalizedString("", comment: ""), NSLocalizedString("", comment: "")]
    var mainViewController: UIViewController!
    var settingViewController : UIViewController!
    var tableViewHeaderView : LeftTableviewHeaderView?
    @IBOutlet weak var tableView : UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "ListTitleTableViewCell", bundle: nil), forCellReuseIdentifier: "listCell")
        
        let views = Bundle.main.loadNibNamed("LeftTableviewHeaderView", owner: self, options: nil)
        if let headerView = views?[0] as? LeftTableviewHeaderView {
            self.tableViewHeaderView = headerView
            self.tableViewHeaderView?.delegate = self
            self.tableView.tableHeaderView = self.tableViewHeaderView;
        }
        
        self.tableView.separatorStyle = .none
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let collectionView = storyboard.instantiateViewController(withIdentifier: "tableViewHeaderView") as! GridViewController
//        self.gridViewController = UINavigationController(rootViewController: collectionView)

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.tableView.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: self.tableView.frame.height)
//        self.imageHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 160)
        self.view.layoutIfNeeded()
    }
}

// MARK : FUNC
extension LeftViewController : LeftTableViewHeaderDelegate {
    func leftTableViewHeaderViewDidPressProfile(view: LeftTableviewHeaderView) {
        if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
            let url = URL(string: ServerManager.getPageUrl(type: .myhome))
            viewController.url = url!
        }
        self.changeViewController(.main)
    }
    
    func leftTableViewHeaderViewDidPressStartTest(view: LeftTableviewHeaderView) {
        if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
            let url = URL(string: ServerManager.getPageUrl(type: .skinReader))
            viewController.url = url!
        }
        self.changeViewController(.main)
    }

}

// MARK: Table View
extension LeftViewController : UITableViewDelegate, UITableViewDataSource {
    // MARK: Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section(rawValue: section)! {
        case .menu: return menuArray.count
        case .myPage: return myPageArray.count
        case .setting: return settingArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Section(rawValue: indexPath.section)! {
        case .menu:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.listCell.rawValue, for: indexPath) as! ListTitleTableViewCell
            let title = menuArray[indexPath.row]
            cell.titleLabel.text = title
            cell.selectionStyle = .none
            return cell
            
        case .myPage:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.listCell.rawValue, for: indexPath) as! ListTitleTableViewCell
            let title = myPageArray[indexPath.row]
            cell.titleLabel.text = title
            cell.selectionStyle = .none
            return cell
            
        case .setting:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.listCell.rawValue, for: indexPath) as! ListTitleTableViewCell
//            let title = settingArray[indexPath.row]
            cell.titleLabel.text = SRUser.instance.isLogin == true ? "로그아웃" : "로그인"
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionLocalizedTitles[section]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var leftMenu = LeftMenu.collection
        
        switch Section(rawValue: indexPath.section)! {
        case .menu:
            
            if indexPath.row == 0 {
                if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                    let url = URL(string: ServerManager.getPageUrl(type: .home))
                    viewController.url = url!
                }
                
            } else if indexPath.row == 1 {
                if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                    let url = URL(string: ServerManager.getPageUrl(type: .movie))
                    viewController.url = url!
                }
            } else if indexPath.row == 2 {
                if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                    let url = URL(string: ServerManager.getPageUrl(type: .magazine))
                    viewController.url = url!
                }
            } else if indexPath.row == 3 {
                if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                    let url = URL(string: ServerManager.getPageUrl(type: .diary))
                    viewController.url = url!
                }
            } else if indexPath.row == 4 {
                if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                    let url = URL(string: ServerManager.getPageUrl(type: .pimples))
                    viewController.url = url!
                }
            } else if indexPath.row == 5 {
                if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                    let url = URL(string: ServerManager.getPageUrl(type: .pores))
                    viewController.url = url!
                }
            }
            
            leftMenu = .main

        case .myPage :
            
            if indexPath.row == 0 {
                if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                    let url = URL(string:ServerManager.getPageUrl(type: .myList))
                    viewController.url = url!
                }
            } else if indexPath.row == 1 {
//                if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
//                    let url = URL(string: "http://app.cosmeticreader.kr/skin_diary/skin_diary/")
//                    viewController.url = url!
//                }
                let settingViewController = SettingViewController.baseSettingViewControllerFromStroyBoard()
                settingViewController.delegate = self
                self.present(settingViewController, animated: true, completion: nil)
                
            }
            
//            if let collectionView = (self.gridViewController as? UINavigationController)?.topViewController as? GridViewController {
//                let collection: PHCollection
//                collection = smartAlbums.object(at: indexPath.row)
//                // configure the view controller with the asset collection
//                guard let assetCollection = collection as? PHAssetCollection
//                    else { fatalError("expected asset collection") }
//                collectionView.fetchResult = PHAsset.fetchAssets(in: assetCollection, options: nil)
//                collectionView.assetCollection = assetCollection
//                collectionView.naviTitle = collection.localizedTitle
//            }

            // get the asset collection for the selected row

            leftMenu = .main
        case .setting:
            
            if indexPath.row == 0 {
                if SRUser.instance.isLogin == true {
                    if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                        //                    let url = URL(string: "http://app.cosmeticreader.kr/skin_diary/skin_diary/")
                        //                    viewController.url = url!
                        viewController.logout()
                    }
                } else {
                    if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                        let url = URL(string:ServerManager.getPageUrl(type: .login))
                        viewController.url = url!
                    }
                }
                

            }
//            if let collectionView = (self.gridViewController as? UINavigationController)?.topViewController as? GridViewController {
//                let collection: PHCollection
//                collection = userCollections.object(at: indexPath.row)
//                // configure the view controller with the asset collection
//                guard let assetCollection = collection as? PHAssetCollection
//                    else { fatalError("expected asset collection") }
//                collectionView.fetchResult = PHAsset.fetchAssets(in: assetCollection, options: nil)
//                collectionView.assetCollection = assetCollection
//                collectionView.naviTitle = collection.localizedTitle
//            }

            // get the asset collection for the selected row

            leftMenu = .main
        }
        self.changeViewController(leftMenu)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
}

extension LeftViewController : SettingViewDelegate {
    func SettingView(view: SettingViewController, IndexPath indexPath: IndexPath) {
        if indexPath.row == 0 {
            
        } else if indexPath.row == 1 {
            if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                let url = URL(string:ServerManager.getPageUrl(type: .notice))
                viewController.url = url!
            }
        } else if indexPath.row == 2 {
            if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                let url = URL(string:ServerManager.getPageUrl(type: .faq))
                viewController.url = url!
            }
        } else if indexPath.row == 3 {
            if let viewController = (self.mainViewController as? UINavigationController)?.topViewController as? MainViewController {
                let url = URL(string:ServerManager.getPageUrl(type: .oneonene))
                viewController.url = url!
            }
        } else if indexPath.row == 4 {
        }
        
        self.changeViewController(.main)
    }
}

extension LeftViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
        StartViewController.httpReqeustAutoLogin { (isSuccess) in
            Alamofire.request(SRUser.instance.image).responseImage { response in
                if let image = response.result.value {
                    self.tableViewHeaderView?.profileButton.setBackgroundImage(image, for: .normal)
                } else {
                    self.tableViewHeaderView?.profileButton.setBackgroundImage(UIImage(named: "profile_default"), for: .normal)
                }
            }
            self.tableViewHeaderView?.conditionLabel.text = SRUser.instance.skintype_text
            self.tableViewHeaderView?.nameLabel.text = SRUser.instance.nickname
            self.tableView.reloadData()
        }

    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

extension LeftViewController : LeftMenuProtocol {
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
//        case .collection:
//            self.slideMenuController()?.changeMainViewController(self.gridViewController, close: true)
        default : break
        }
    }
}

