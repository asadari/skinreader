//
//  ListTileSubTitleTableViewCell.swift
//  SkinReader
//
//  Created by Gwanho on 2017. 2. 1..
//  Copyright © 2017년 skinReader. All rights reserved.
//

import UIKit

class ListTileSubTitleTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var subtitleLabel : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
