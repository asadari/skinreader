//
//  AppDelegate.swift
//  SkinReader
//
//  Created by Gwanho on 2017. 1. 11..
//  Copyright © 2017년 skinReader. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let isInitInstall = UserDefaults.standard.bool(forKey: "isInitInstall")
        if isInitInstall == false {
            UserDefaults.standard.set("true", forKey: "isInitInstall")
            UserDefaults.standard.synchronize()
            SRUtils.resetKeyChainData()
        }
        
        //Firebase
        
        if #available(iOS 10, *) {
            
            //Notifications get posted to the function (delegate):  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: () -> Void)"
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                
                guard error == nil else {
                    //Display Error.. Handle Error.. etc..
                    return
                }
                
                if granted {
                    //Do stuff here..
                    
                }
                else {
                    //Handle user denying permissions..
                    //                    SRUser.instance.push = "off"
                }
                application.registerForRemoteNotifications()
            }
            
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
        FIRApp.configure()
        
        SlideMenuOptions.simultaneousGestureRecognizers = false
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
//        let url = URL(string: STR_baseUrl)
//        mainViewController.url = url!
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        UINavigationBar.appearance().tintColor = UIColor.red
        
        leftViewController.mainViewController = nvc
        
        let slideMenuController = ContainerViewController(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = leftViewController
        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
        
        if launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] != nil {
            if let launchOptions = launchOptions {
                if var userInfo = launchOptions[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable : Any]{
                    // Do what you want to happen when a remote notification is tapped.
                    //                    self.actionNotiAlert(userInfo: userInfo as! [AnyHashable : Any]) { (isOk) in
                    //                    }
                    
                    
                }
            }
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult ) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //        print("Message ID: \(userInfo["gcm.message_id"]!)")
        
        // Print full message.
        //        print("%@", userInfo)
        
        //        self.actionNotiAlert(userInfo: userInfo) { (isOk) in
        //
        //        }
        
        
        let state = application.applicationState
        
        if state == .active {
            self.actionNotiAlert(userInfo: userInfo) { (isOk) in
                
            }
        } else {
            var userInfo = userInfo
            if let gcmId = userInfo["gcm.message_id"] as? String {
                if let url = userInfo["url"] as? String {
                    userInfo["webUrl"] = url
                    SRUser.instance.initUrlString = url
                    
                }
            } else {
                if let aps = userInfo["aps"] as? Dictionary<AnyHashable, Any>  {
                    if let url = aps["url"] as? String {
                        userInfo["webUrl"] = url
                        SRUser.instance.initUrlString = url
                    }
                }
            }
            
//            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(3.0 * Double(NSEC_PER_SEC)))) {
                let notiCenter = NotificationCenter.default
                notiCenter.post(name: .remotePushNotiName , object: nil, userInfo: userInfo)
//            }
        }
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        self.actionNotiAlert(userInfo: userInfo) { (isOk) in
            
        }
    }
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        SRUser.instance.userPushKey = deviceTokenString
        print(deviceTokenString)
        #if DEBUG
            FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .sandbox)
        #else
            FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: .prod)
        #endif
        
    }
    
    func tokenRefreshNotificaiton(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            SRUser.instance.userPushKey = refreshedToken
            
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }
    
    func setRemotePush(userInf : Dictionary<AnyHashable,Any>) {
        
    }
}


//MARK : FUNCTION
extension AppDelegate {
    func actionNotiAlert( userInfo : [AnyHashable: Any] , Compelte callback: @escaping (Bool) -> Void) {
        
        
        var userInfo = userInfo
        
        if let gcmId = userInfo["gcm.message_id"] as? String {
            let alertController = UIAlertController(title: userInfo["title"] as? String, message: userInfo["content"] as? String, preferredStyle: .alert)
            
            if let url = userInfo["url"] as? String {
                userInfo["webUrl"] = url
            }
            
            let okAction = UIAlertAction(title: NSLocalizedString("확인", comment: ""), style: .default) { (action) in
                let notiCenter = NotificationCenter.default
                notiCenter.post(name: .remotePushNotiName , object: nil, userInfo: userInfo)
                
                callback(true)
                
            }
            
            let cancel = UIAlertAction(title: NSLocalizedString("취소", comment: ""), style: .cancel) { (action) in
                callback(false)
            }
            
            alertController.addAction(okAction)
            alertController.addAction(cancel)
            
            let vc = self.window?.rootViewController
            vc?.present(alertController, animated: true, completion: nil)
        } else {
            
            if let aps = userInfo["aps"] as? Dictionary<AnyHashable, Any>  {
                
                let alertController = UIAlertController(title: aps["title"] as? String, message: aps["message"] as? String, preferredStyle: .alert)
                
                if let url = aps["url"] as? String {
                    userInfo["webUrl"] = url
                }
                
                let okAction = UIAlertAction(title: NSLocalizedString("확인", comment: ""), style: .default) { (action) in
                    let notiCenter = NotificationCenter.default
                    notiCenter.post(name: .remotePushNotiName , object: nil, userInfo: userInfo)
                    
                    callback(true)
                    
                }
                
                let cancel = UIAlertAction(title: NSLocalizedString("취소", comment: ""), style: .cancel) { (action) in
                    callback(false)
                }
                
                alertController.addAction(okAction)
                alertController.addAction(cancel)
                
                let vc = self.window?.rootViewController
                vc?.present(alertController, animated: true, completion: nil)
            }
            
        }
    }
}

extension UIApplication{
    
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
        if let slide = viewController as? SlideMenuController {
            return topViewController(slide.mainViewController)
        }
        return viewController
    }
}


